new Vue({
	el:'#app',
	data:{
		bfoIncome:0,
		oldSL:4,
		ylSL:4,
		sySL:4,
		zfSL:4,
	},
	computed:{
		bfoRes:function(){
			return this.bfoIncome*(1-(this.oldSL+this.ylSL+this.sySL+this.zfSL)/100)
		},
		grsds:function(){
			let sl = 0
			let sskcs = 0
			let basIncom = this.bfoRes-3500
			if(basIncom<=0){
				return 0
			}
			if (basIncom<=1500) {
				sl = 3
				sskcs = 0
			} else if(basIncom<=4500){
				sl = 10
				sskcs = 105
			}else if(basIncom<=9000){
				sl = 20
				sskcs = 555
			}else if(basIncom<=35000){
				sl = 25
				sskcs = 1005
			}else if(basIncom<=55000){
				sl = 30
				sskcs = 2755
			}else if(basIncom<=80000){
				sl = 35
				sskcs = 5505
			}else{
				sl = 45
				sskcs = 13505
			}
			return basIncom*sl/100-sskcs
		},
		shgz:function(){
			return this.bfoRes-this.grsds
		}
	},
	filters:{  
		dataFormatter(v){
            return (v).toFixed(2)
        }
    }
})
