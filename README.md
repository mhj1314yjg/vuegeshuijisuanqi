## Vue个税计算器
### data
0. bfoIncome 原始工资
1. oldSL 养老税率
2. ylSL 医疗税率
3. sySL 失业税率
4. zfSL 住房税率
### computed 
1. bfoRes 税前工资
2. grsds 个人所得税
3. shgz 税后工资
### filters 
1. dataFormatter 数据格式化
